#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <gtest/gtest.h>

using namespace std;

class FileRecoder {
private:
    string line; //очередная строка из файла
    string path ; //путь до файла
    ifstream file; //поток ввода
    bool status;
public:
    FileRecoder(string newPath) {
        line = "";
        path = "\0";
        path = newPath;
        file.open(path);
        if(!file.is_open())
            status = false; //cannot open the file
        else
            status = true; //the file open
    }
    bool nextLine() {
        line = "\0";
        if (!getline(file, line).eof())
            return true;
        else
            return false;
    }
    string gettingLine(){ return line; }
    bool isOpen() { return status; }
    ~FileRecoder() { file.close(); }
};

class TokenService {
private:
    bool ifSymbol(char symb) {
        if((symb >= 'a' && symb <= 'z') ||(symb >= '0' && symb <= '9'))
            return true;
        else
            return false;
    }
public:
    vector<string> splitLine(string str){
        vector<string> wordList;
        string word = "\0";
        for(int i = 0; i <= (int)str.size(); i++) {
            char symbol = (char)tolower((char)str[i]);  //сводим символ к нижнему регистру
            if(ifSymbol(symbol)) //если символ это буква или цифра то добавляем его в слово
                word += symbol;
            else {
                if(word[0] != '\0') {
                    word += "\0";
                    wordList.push_back(word);
                }
                word = "\0";
            }
        }
        return wordList;
    }
};


class WordStatService {
private:
    map <string, int> dictionary;
    int countWord;
    vector< pair<string, pair<int, double>> > table;

    void getCountWord() {
        map <string, int> :: iterator it = dictionary.begin();
        for(int i = 0; it != dictionary.end(); i++, it++)
            countWord += it->second;
    }
    float getFrequency(int const num) { return ((float)num)/((float)countWord)*100.0; }
    void setTable(){
        table.clear();
        map <string, int> :: iterator it = dictionary.begin();

        for(int i = 0; it != dictionary.end(); i++, it++) {
            table.push_back(make_pair(it->first, make_pair(it->second, getFrequency(it->second) ) ) );
        }

        sort(table.begin(), table.end(), [](auto &left, auto &right) -> bool {
            return left.second.first > right.second.first;
        });
    }

public:
    WordStatService(){countWord = 0;}
    void insertWords(vector<string> words){
        map <string, int> :: iterator it = dictionary.begin();
        for(int i = 0; i < (int)words.size(); i++) {
            it = dictionary.find(words[i]);
            if(dictionary.end() == it)
                dictionary.insert(pair<string, int>(words[i], 1));
            else
                dictionary.insert(pair<string, int>(words[i], ++it->second));
        }
    }
    vector<pair<string, pair<int, double>>>  getTable() {
        getCountWord();
        setTable();
        return table;
    }
};

class ReportService {
private:
    string path;
    ofstream file;
    bool status;
public:
    ReportService(string newPath) {
        path = newPath;
        file.open(path);
        if(!file.is_open())
            status = false; //cannot open the file
        else
            status = true; //the file open
    }
    void printTab(vector< pair<string, pair<int, double>>> table){
        int size = table.size();
        for(int i = 0; i < size; i++)
            file <<  table[i].first << ";" << table[i].second.first << ";" << table[i].second.second << '\n';
    }
    bool isOpen() { return status; }
    ~ReportService() { file.close(); }
};

int main(int arg, char* argv[]) {
    if(arg != 3) {
        cout << "incorrect number of arguments";
        return 0;
    }

    FileRecoder FileRec(argv[1]);
    if(!FileRec.isOpen())
        cout << "Cannot open the file.\n";
    else {
        TokenService wordList;

        WordStatService dict;

        while (FileRec.nextLine())
            dict.insertWords(  wordList.splitLine( FileRec.gettingLine() ) );

        ReportService FileOut(argv[2]);
        if(!FileOut.isOpen())
            cout << "Cannot open the file.\n";
        else
            FileOut.printTab(dict.getTable());
    }

    return 0;
}

//
//int main(int argc, char* argv[]) {
//    return RUN_ALL_TESTS();
//}
//
//TEST(FileRecorderTests, ConstructorTests) {
//    FileRecoder testRec1("C:\\Programming\\oop-19210-gayfutdinov\\19210 Gayfutdinov lab0b\\testFile1.txt");
//    ASSERT_TRUE(testRec1.isOpen());
//    FileRecoder  testRec2("none");
//    ASSERT_TRUE(!testRec2.isOpen());
//}
//TEST(FileRecorderTests, NextLineTests) {
//    FileRecoder testRec1("C:\\Programming\\oop-19210-gayfutdinov\\19210 Gayfutdinov lab0b\\testFile1.txt");
//    ASSERT_TRUE(testRec1.nextLine());
//    ASSERT_TRUE(!testRec1.nextLine());
//}
//TEST(FileRecoderTests, GettingLineTests){
//    FileRecoder testRec1("C:\\Programming\\oop-19210-gayfutdinov\\19210 Gayfutdinov lab0b\\testFile1.txt");
//    testRec1.nextLine();
//    ASSERT_EQ(testRec1.gettingLine(), "One line.");
//    testRec1.nextLine();
//    ASSERT_EQ(testRec1.gettingLine(), "");
//}
//
//TEST(TokenServiceTests, SplitLineTests){
//    TokenService testTS;
//    //testTS.splitLine("\n\\.,^*:;()!?\"\'\t");
//    ASSERT_TRUE(testTS.splitLine("\n\\.,^*:;()!?\"\'\t").empty());
//    //testTS.splitLine("");
//    ASSERT_TRUE(testTS.splitLine("").empty());
//
//    vector <string> words = testTS.splitLine("ZZZzzzZZZzZZ.  zzzzzzzzzzzzzz!");
//    ASSERT_TRUE( 2 == words.size());
//    ASSERT_EQ(words[0], "zzzzzzzzzzzz");
//    ASSERT_EQ(words[1], "zzzzzzzzzzzzzz");
//}
//
//TEST(WordStatServiceTests, InsertWordsTests) {
//    vector<string> testVect1;
//    testVect1.push_back("one");
//    testVect1.push_back("two");
//    testVect1.push_back("three");
//    testVect1.push_back("four");
//    testVect1.push_back("five");
//    testVect1.push_back("six");
//    WordStatService testDict1;
//    testDict1.insertWords(testVect1); testVect1.clear();
//
//
//    ASSERT_TRUE(testDict1.getTable().size() == 6);
//    testVect1.push_back("one");
//    testVect1.push_back("one");
//    testVect1.push_back("one");
//    testVect1.push_back("one");
//    testVect1.push_back("one");
//    testVect1.push_back("zzzz");
//    testVect1.push_back("yyyy");
//    testDict1.insertWords(testVect1); testVect1.clear();
//    ASSERT_TRUE(testDict1.getTable().size() == 8);
//    ASSERT_TRUE(testDict1.getTable()[0].second.first == 6);
//}
